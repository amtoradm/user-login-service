package com.retelzy.login.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.retelzy.login.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);

	@Query(value = "select * from user where email=?1", nativeQuery = true)
	User getUserInfo(String email);

	@Query(value = "select * from user where email=?1", nativeQuery = true)
	User isUserActive(String email);
}
