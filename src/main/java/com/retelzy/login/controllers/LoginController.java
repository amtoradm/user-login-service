package com.retelzy.login.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.retelzy.login.entity.User;
import com.retelzy.login.payload.request.LoginRequest;
import com.retelzy.login.payload.response.JwtResponse;
import com.retelzy.login.repository.UserRepository;
import com.retelzy.login.security.jwt.JwtUtils;
import com.retelzy.login.security.services.UserDetailsImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class LoginController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;

	@PostMapping("/login")
	public ResponseEntity<JwtResponse> authenticateUser(@RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
		
		User user = userRepository.getUserInfo(loginRequest.getEmail());
		roles.add(user.getRoles());
		return ResponseEntity.ok(
				new JwtResponse(jwt, 
						userDetails.getId(), 
						userDetails.getFirstname(), 
						userDetails.getLastname(), 
						userDetails.getEmail(), 
						userDetails.getRoles(), 
						roles));

	}
	
	@GetMapping(value = "/checkAccount")
	public ResponseEntity<?> checkAccount(@RequestParam("email") String email) {
		User user = userRepository.getUserInfo(email);
		if (user != null) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping(value = "/checkActiveAccount")
	public ResponseEntity<?> checkActiveAccount(@RequestParam("email") String email) {
		User user = userRepository.isUserActive(email);
		if(user!=null) {
			if (user.isEnabled() == true) {
				return new ResponseEntity<>(user, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(user.isEnabled(), HttpStatus.OK);
			}
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

}
