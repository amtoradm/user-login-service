package com.retelzy.login.service;

import com.retelzy.login.entity.User;

public interface UserService {

	User findUserByEmail(String email);
}
